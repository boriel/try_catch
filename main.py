#!/usr/bin/env python
# -*- coding: utf-8 -*-

from board import Board
from inputparser import InputParser
from algorithm import BacktrackingGenerator
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='The Chess challenge (see README file for more '
                                                 'info')
    parser.add_argument('-n', '--count', dest='show', action='store_true',
                        help='shows the total number of possible solutions')
    parser.add_argument('-c', '--show_examined', dest='examined', action='store_true',
                        help="shows the total number of examined solutions")
    parser.add_argument('-s', '--silent', dest='silent', action='store_true',
                        help="silent mode: does not print solutions")

    args = parser.parse_args()
    parser = InputParser(input())
    algorithm = BacktrackingGenerator(Board(parser.M, parser.N),
                                      parser.pieces_list)
    for board in algorithm:
        if not args.silent:
            print(board)

    if args.show:
        print('Valid solutions found:', algorithm.valid_solutions)

    if args.examined:
        print('Total positions examined:', algorithm.examined_positions)
