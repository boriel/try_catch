
__doc__ = """ Contains several algorithms to solve the problem.
          """

from position import PositionSequencer
from board import Board, Empty
from piece import Piece


class BacktrackingGenerator:
    """ This class implements an iterator over all
    valid board configurations.
    """
    def __init__(self, board, piece_types):
        """
        :param board: A board object. E.g. Board(5, 5)
        :param piece_types: An iterable of Piece classes
        (e.g. [King, King, Rook])
        """
        assert isinstance(board, Board), "board must be a Board instance"
        assert all(issubclass(piece_type, Piece) for piece_type in piece_types)
        self.board = board
        self.piece_types = piece_types
        self.valid_solutions = 0
        self.examined_positions = 0

    def __iter__(self):
        if self.piece_types:
            yield from self._valid_boards(self.board, self.piece_types, 0, 0)

    def _valid_boards(self, board, pieces, row, col):
        """ Recursive function (generator) which places a piece and
        then tries to put the remaining ones.
        When no more pieces remaining we've found a valid solution.

        WARNING: To avoid duplicated solutions, this algorithm *requires*
        the list of pieces to be grouped, that is, for example:
          pieces parameter should be either [Rook, Rook, Bishop]
          or [Bishop, Rook, Rook] but not [Rook, Bishop, Rook].

        :param board: a Board(M, N) instance
        :param pieces: a list of Piece subclasses
        (Knight, King, Queen, Rook, Bishop). They can repeat. (see WARNING)
        :param row: row to start searching for valid positions.
        :param col: column to start searching for valid positions.
        :yields: all valid board configurations.
        """
        occupied = {piece.position for piece in board.pieces}

        for pos in PositionSequencer(board, row, col):
            self.examined_positions += 1
            if board.at(pos) is not Empty or pos in board.attacked:
                continue

            board_ = board.copy()
            board_.place(pieces[0], pos)

            if occupied.intersection(board_.at(pos).attacked_positions):
                continue  # not valid. The new piece is attacking previous ones

            if len(pieces) - 1 > board_.size - len(board_.attacked):
                continue  # No more valid solutions possible

            if len(pieces) == 1:
                self.valid_solutions += 1
                yield board_  # This is a solution
            else:
                if pieces[0] is pieces[1]:  # The same type of piece?
                    next_row, next_col = pos.row, pos.col
                else:
                    next_row, next_col = 0, 0
                yield from self._valid_boards(board_, pieces[1:], next_row,
                                              next_col)
