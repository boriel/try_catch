#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase
from algorithm import BacktrackingGenerator
from board import Board
from piece import Queen
from piece import Knight


class TestBacktrackingGenerator(TestCase):
    def test__valid_boards_no_solution(self):
        alg = BacktrackingGenerator(Board(3, 3), [Queen] * 3)
        self.assertEqual(len([x for x in alg]), 0)
        self.assertEqual(alg.valid_solutions, 0)

    def test__valid_boards_no_pieces(self):
        alg = BacktrackingGenerator(Board(3, 3), [])
        self.assertEqual(len([x for x in alg]), 0)
        self.assertEqual(alg.valid_solutions, 0)

    def test__valid_boards_4_4_5_knigth(self):
        alg = BacktrackingGenerator(Board(3, 3), [Knight] * 5)
        self.assertEqual([repr(x) for x in alg],
                         ["N N N N N", " N NNN N "])
        self.assertEqual(alg.valid_solutions, 2)
