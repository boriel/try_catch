
import re

from piece import Knight
from piece import Bishop
from piece import Rook
from piece import Queen
from piece import King


class InputParser:
    """ Implements a (very) simple parser using Regular Expressions,
    to parse sentences like "4×4 board containing 2 Rooks and 4 Knights"
    """
    piece_names = {
        'king': King,
        'queen': Queen,
        'rook': Rook,
        'knight': Knight,
        'bishop': Bishop
    }

    class SizeNotSpecifiedError(Exception):
        def __str__(self):
            return "Board size not specified"

    class PiecesNotSpecifiedError(Exception):
        def __str__(self):
            return "No pieces specified"

    def __init__(self, string):
        """
        :param string: String to parse
        """
        string = string.lower()

        size_RE = re.compile(r'(\d+x\d+)[ \t]board')
        parse_size = size_RE.search(string)
        if parse_size is None:
            raise InputParser.SizeNotSpecifiedError
        self.M, self.N = tuple(int(x)
                               for x in parse_size.groups()[0].split('x'))
        pieces_RE = re.compile(r'(\d+[ \t]+(?:%s))' %
                               '|'.join(x.__name__
                                        for x in self.piece_names.values()),
                               re.IGNORECASE)
        pieces_list = pieces_RE.findall(string)
        if not pieces_list:
            raise InputParser.PiecesNotSpecifiedError

        self.pieces_list = [self.piece_names[piece]
                            for n, piece_type in [re.split('[ \t+]', x)
                                                  for x in pieces_list]
                            for piece in int(n) * [piece_type]]

