
__doc__ = """ MixIns for several piece behaviour (diagonal, horizontal +
horizontal and knight-like movement.
"""


from .piece import Piece
from position import Direction


class LimitedMovementMixIn(Piece):
    """ Defines a MixIn for pieces which can limit their movement to n squares
    """
    movement_limit = None  # If set to 1, only distance 1 will be considered


class HoriVertMixIn(LimitedMovementMixIn):
    """ Defines an ABC for a piece that moves vertically or horizontally
    """
    @property
    def attacked_horivert_positions(self):
        result = set()

        for direction in (Direction.North, Direction.South, Direction.East,
                          Direction.West):
            count = 0
            pos = self.position

            while (self.movement_limit is None or count < self.movement_limit) \
                    and pos.go(direction) in self.board:
                pos = pos.go(direction)
                result.add(pos)
                count += 1

        return result


class DiagonalMixIn(LimitedMovementMixIn):
    """ Implements the Diagonal movements
    """
    @property
    def attacked_diagonal_positions(self):
        result = set()

        for direction in (Direction.NorthEast, Direction.SouthEast,
                          Direction.NorthWest, Direction.SouthWest):
            count = 0
            pos = self.position

            while (self.movement_limit is None or count < self.movement_limit) \
                    and pos.go(direction) in self.board:
                pos = pos.go(direction)
                result.add(pos)
                count += 1

        return result


class KnightMixIn(Piece):
    """ This MixIn implements the Knight movement
    """
    @property
    def attacked_knight_positions(self):
        result = set()

        for dirA in (Direction.North, Direction.South,
                     Direction.East, Direction.West):
            pos_a = self.position.go(dirA).go(dirA)
            for dirB in (
                    Direction.North,
                    Direction.South
            ) if dirA in (Direction.East, Direction.West) else (
                    Direction.East, Direction.West
            ):
                pos_b = pos_a.go(dirB)
                if pos_b in self.board:
                    result.add(pos_b)

        return result
